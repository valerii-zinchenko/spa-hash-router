module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		umd: {
			pkg: {
				options: {
					src: 'lib/HashRouter.js',
					dest: 'dest/HashRouter.js',
					template: 'umd.hbs',
					objectToExport: 'HashRouter'
				}
			}
		},

		uglify: {
			options: {
				banner: '//\#Router v<%= pkg.version %>\n' +
						'//Copyright (c) 2015-2018  <%= pkg.author %>\n' +
						'//Licensed under MIT (http://valerii-zinchenko.gitlab.io/spa-hash-router/blob/master/LICENSE.txt)\n' +
						'//All source files are available at: http://gitlab.com/valerii-zinchenko/spa-hash-router\n',
			},
			dist: {
				files: {
					'dest/HashRouter.min.js': 'dest/HashRouter.js'
				}
			}
		},

		copy: {
			test: {
				expand: true,
				cwd: './node_modules',
				src: [
					'mocha/mocha.js',
					'mocha/mocha.css',
					'chai/chai.js',
					'sinon/pkg/sinon.js'
				],
				dest: 'test/lib',
				flatten: true
			}
		},

		jsdoc: {
			options: {
				configure: 'jsdoc.conf.json',
			},
			doc: {
				src: './lib/HashRouter.js',
				options: {
					package: 'package.json'
				}
			},
			nightly: {
				src: './lib/HashRouter.js'
			}
		}
	});

	grunt.registerTask('build', ['umd', 'uglify']);
};
