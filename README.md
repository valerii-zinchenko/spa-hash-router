# \#Router

\#Router is a simple hash router for a Single Page Application (SPA). The implementation follows the [Mobile Web Application Best Practices](http://www.w3.org/TR/2010/REC-mwabp-20101214/#bp-presentation-fragment) from [W3C](http://www.w3.org).

A route can be defined by using named parts like `:name`, or by using a RegExp with captured groups like `(\\w+)`. Remember to allways escape a `\` character and use the round braces to capture variable parts of a route.
Please note, that appropriate methods will be executed only on the first matched route. So plan the order of similar routes by importance in descending order.

\#Router also processes [queries](https://github.com/valerii-zinchenko/hash-router/wiki/Definitions#query-types). It converts them into an object and pass it into each method as the last argument. Even if there is no queries an empty object will be passed as the last argument, so you don't need every time to check if it is defined or undefined.


## Requirements

* [Function.prototype.bind](http://caniuse.com/#feat=es5)
* [Array.prototype.forEach](http://caniuse.com/#feat=es5)
* [hashchange event](http://caniuse.com/#search=hashchange)

If this library is planned to be used in some legacy environment then please add required plyfills. Here is a good resource: https://github.com/es-shims/es5-shim


## Installation

Via [NPM (Node Package Manager)](https://github.com/npm/npm)
```
$ npm install spa-hash-router --save
```

Library files:

* `dest/HashRouter.js` - not minified library
* `dest/HashRouter.min.js` - minified library

The library can be used:

* as an AMD module
* or it can be get from the global variable under the name `HashRouter`


## Simple usage example

```js
new HashRouter({
    prefix: '!',
    fallbackRoute: 'home',
    routes: {
        home: function() {
            console.log('I am home');
        },
        about: [
            function(){
                console.log('do something before');
            },
            function(){
                console.log('show about');
            },
            function(){
                console.log('do something after');
            }
        ]
    }
});
```


## Useful links

* [wiki](https://gitlab.com/valerii-zinchenko/spa-hash-router/wikis/home)
* [API](http://valerii-zinchenko.gitlab.io/spa-hash-router/doc/index.html)
* [Unit tests](http://valerii-zinchenko.gitlab.io/spa-hash-router/test/index.html)
